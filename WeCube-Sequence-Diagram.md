WeCube Sequence Diagram
```mermaid
sequenceDiagram    
    participant Portal as WeCube-Portal <br/>(Browser)
    participant Core as Wecube Core <br/>(Web Server) <br/> (10.0.0.1)
    participant SM as Plugin-SM<br/>(Restful API Server) <br/>(service-management 10.0.0.2)
    
    SM ->>+ Core: Register sm-API= /query and sm-static=/report.html
    Note left of Core: Save to Core
    
    Portal ->>+ Core: HTTP Request http://10.0.0.1/plugins
    Core -->>- Portal : Response pluginName=sm,sm-API=/query,sm-static=/report.html
    Portal ->>+ Core: HTTP Request http://10.0.0.1/sm/report.html
    Core ->>+ SM: HTTP Request http://10.0.0.2/report.html
    SM -->>- Core: HTTP Response
    Core -->>- Portal: HTTP Respose
    Note left of Portal: Load report.html
    
    Portal ->>+ Core: HTTP Request http://10.0.0.1/sm/query
    Core ->>+ SM: HTTP Resquest http://10.0.0.2/query
    SM -->>- Core: HTTP Response 
    Core -->>- Portal: HTTP Response
    
```